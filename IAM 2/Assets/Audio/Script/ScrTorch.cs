﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrTorch : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Event;

    void Start()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(Event, gameObject);
    }
}
