﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class ScriptFootsteps : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string Event;
    public vThirdPersonInput tpInput;

    FMOD.Studio.EventInstance ftInstance;


    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }
    void FootStep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            ftInstance = FMODUnity.RuntimeManager.CreateInstance(Event);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(ftInstance,GetComponent<Transform>(), GetComponent<Rigidbody>());
            ftInstance.start();
            ftInstance.release();
        }

    }
}
